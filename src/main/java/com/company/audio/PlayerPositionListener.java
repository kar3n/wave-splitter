package com.company.audio;

public interface PlayerPositionListener {

	void positionChanged(long newPosition);
	
}
