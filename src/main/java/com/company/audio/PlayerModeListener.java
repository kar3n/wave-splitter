package com.company.audio;

public interface PlayerModeListener {

	void playerStarted();

	void playerStopped();
}
